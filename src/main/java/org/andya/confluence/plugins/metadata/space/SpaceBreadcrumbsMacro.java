/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.andya.confluence.utils.ContentUtils;
import org.andya.confluence.utils.MacroUtils;

import java.util.Map;

/**
 *
 */
public class SpaceBreadcrumbsMacro extends AbstractSpaceMetadataMacro {
	public static final String SHOW_DASHBOARD_AND_HOME = "dashboard+home";
	public static final String SHOW_DASHBOARD_ONLY = "dashboard";
	public static final String SHOW_HOME_ONLY = "home";

	public SpaceBreadcrumbsMacro() {
		super("space-breadcrumbs");
	}

	public boolean isInline() {
		return false;
	}

	public boolean hasBody() {
		return false;
	}

	public RenderMode getBodyRenderMode() {
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body,
			RenderContext renderContext) throws MacroException {
		try {
			String pageName = MacroUtils.getStringParameter(parameters, "0", "@self");
			String show = MacroUtils.getStringParameter(parameters, "1", false);
			boolean showDashboard = SHOW_DASHBOARD_AND_HOME.equals(show)
					|| SHOW_DASHBOARD_ONLY.equals(show);
			boolean showHome = SHOW_DASHBOARD_AND_HOME.equals(show)
					|| SHOW_HOME_ONLY.equals(show);
			Page page = getPage(renderContext, pageName, false);
			Space space;
			String title;
			boolean isHome;
			if (page != null) {
				space = page.getSpace();
				title = page.getTitle();
				isHome = ContentUtils.isHomePage(page);
			} else {
				String[] pageSections = pageName.split(":");
				String spaceKey = pageSections[0];
				title = pageSections[1];
				space = getSpaceManager().getSpace(spaceKey);
				isHome = false;
			}
			User user = getUser();
			SpaceMap spaceMap = SpaceMap.get(user, getContentService());
			SpaceHierarchy hierarchy = spaceMap.getSpaceHierarchy(space);
			StringBuffer html = new StringBuffer();
			html.append("<span class=\"topBarDiv\">");
			if (showDashboard)
				html.append("<a href=\"/dashboard.action\">Dashboard</a> &gt; ");
			writeSpaceBreadcrumbs(html, hierarchy, !(isHome && !showHome), renderContext);
			if (!isHome || showHome) {
				if (page != null)
					writePageBreadcrumbs(html, page, false, showHome, renderContext);
				else
					html.append(title);
			}
			html.append("</span>");
			return html.toString();
		} catch (Exception e) {
			return MacroUtils.showRenderedExceptionString(parameters,
					"Unable to render macro", e);
		}
	}

	private void writeSpaceBreadcrumbs(StringBuffer html,
			SpaceHierarchy hierarchy, boolean linkSpace, RenderContext renderContext) throws MacroException {
		SpaceHierarchy parentHierarchy = hierarchy.getParent();
		if (parentHierarchy != null)
			writeSpaceBreadcrumbs(html, parentHierarchy, true, renderContext);
		Space space = hierarchy.getSpace();
		if (space != null) {
			if (linkSpace) {
				html.append("<a href='");
				html.append(renderContext.getSiteRoot() + space.getUrlPath());
				html.append("'>");
			}
			html.append(space.getName());
			if (linkSpace) {
				html.append("</a>");
				html.append(" &gt; ");
			}
		}
	}

	private void writePageBreadcrumbs(StringBuffer html, Page page,
			boolean linkPage, boolean showHome, RenderContext renderContext) throws MacroException {
		if (!showHome && ContentUtils.isHomePage(page))
			return;
		Page parentPage = page.getParent();
		if (parentPage != null)
			writePageBreadcrumbs(html, parentPage, true, false, renderContext);
		if (linkPage) {
			html.append("<a href='");
			html.append(renderContext.getSiteRoot() + page.getUrlPath());
			html.append("'>");
		}
		html.append(page.getTitle());
		if (linkPage) {
			html.append("</a>");
			html.append(" &gt; ");
		}
	}
}
