/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.blogs;

import com.atlassian.confluence.pages.BlogPost;
import org.andya.confluence.plugins.metadata.space.AbstractSpaceMetadataMacro;
import org.andya.confluence.plugins.metadata.renderers.MetadataRenderer;
import org.andya.confluence.plugins.metadata.MetadataUtils;
import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.model.MetadataSortOrder;
import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.utils.ContentService;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.PageManager;

import java.util.*;

/**
 * Macro allowing metadata from a space to be shown on the current page.
 */
public class BlogPostsReportMacro extends AbstractSpaceMetadataMacro {
	public BlogPostsReportMacro() {
		super("blog-posts-report");
	}

	public boolean isInline() {
		return false;
	}

	public boolean hasBody() {
		return false;
	}

	public RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
		try {
			String commaDelimitedColumns = MacroUtils.getStringParameter(parameters, "0", true);
			int maxResults = MacroUtils.getIntParameter(parameters, MAX_RESULTS_PARAMETER, DEFAULT_MAX_RESULTS);
			List<MetadataContent> contents = getMatchingBlogPosts(parameters, renderContext, true);
			String[] columns = commaDelimitedColumns.split(",");
			String commaDelimitedTotals = MacroUtils.getStringParameter(parameters, TOTALS_PARAMETER, "");
			MetadataRenderer renderer = getMetadataRenderer(parameters);
			if (contents.size() > maxResults)
				contents = contents.subList(0, maxResults);
			return renderer.render(renderContext, contents, maxResults, columns, commaDelimitedTotals, "Space");
		} catch (Exception e) {
			return MacroUtils.showRenderedExceptionString(parameters, "Unable to render macro", e);
		}
	}

	/**
	 * Returns a list of matching spaces based on the parameters to the macro.
	 * @param parameters The macro's parameters.
	 * @param renderContext The render context.
	 * @param ordered If true then the results should be ordered according to the sort parameters.
	 * @return A list of matching spaces
	 * @throws com.atlassian.renderer.v2.macro.MacroException Thrown if any of the parameters are inappropriate.
	 */
	@SuppressWarnings("unchecked")
	public List<MetadataContent> getMatchingBlogPosts(Map parameters, RenderContext renderContext, boolean ordered) throws MacroException {
		String spaceKey = MacroUtils.getStringParameter(parameters, SPACE_PARAMETER, null);
		SpaceManager spaceManager = getSpaceManager();
		PageManager pageManager = getPageManager();
		Space space = getSpace(renderContext);
		if (spaceKey != null)
			space = spaceManager.getSpace(spaceKey);
		List blogPosts = pageManager.getBlogPosts(space, false);
		// todo: handle blog post labels

		// sort the entities if required
		List<MetadataContent> contents = MetadataUtils.getMetadataContent(getContentService(), blogPosts);
		if (ordered) {
			String commaDelimitedSort = MacroUtils.getStringParameter(parameters, SORT_PARAMETER, "");
			MetadataSortOrder[] sortOrders = MetadataSortOrder.getMetadataSortOrders(commaDelimitedSort);
			Comparator<MetadataContent> comparator = MetadataSortOrder.getComparator(sortOrders, ContentService.BLOG_POST_KEY);
			Collections.sort(contents, comparator);
		}
		return contents;
	}
}
