/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.UserManager;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentService;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;


/**
 * This extractor adds metadata from any page to the Confluence search index.
 */
public class MetadataExtractor implements Extractor {
	private ContentPropertyManager contentPropertyManager;
	private SpaceManager spaceManager;
	private UserManager userManager;
	private FormatSettingsManager formatSettingsManager;

	public void addFields(Document document, StringBuffer defaultSearchableText, Searchable searchable) {
		if (searchable instanceof ContentEntityObject) {
			ContentEntityObject ceo = (ContentEntityObject)searchable;
			ContentPropertyManager contentPropertyManager = getContentPropertyManager();
			SpaceManager spaceManager = getSpaceManager();
			UserManager userManager = getUserManager();
			FormatSettingsManager formatSettingsManager = getFormatSettingsManager();
			ContentService contentService = new ContentService(contentPropertyManager, spaceManager, userManager,
					formatSettingsManager);
			String[] metadataNames = MetadataUtils.getMetadataNames(contentService, ceo);
			if (metadataNames != null) {
				for (int i = 0; i < metadataNames.length; i++) {
					String name = metadataNames[i];
					MetadataValue value = MetadataUtils.getMetadataValue(contentService, ceo, name, "");
					if (value != null) {
						defaultSearchableText.append(value.getWikiSnippet()).append(" ");
						document.add(new TextField(name, value.getWikiSnippet(), Field.Store.YES));
					}
				}
			}
		}
	}

	public ContentPropertyManager getContentPropertyManager() {
		return contentPropertyManager;
	}

	public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
		this.contentPropertyManager = contentPropertyManager;
	}

	public SpaceManager getSpaceManager() {
		return spaceManager;
	}

	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public FormatSettingsManager getFormatSettingsManager() {
		return formatSettingsManager;
	}

	public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager) {
		this.formatSettingsManager = formatSettingsManager;
	}
}
