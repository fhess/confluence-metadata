package org.andya.confluence.plugins.metadata;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;

import java.util.Map;

/**
 * Created 12-02-26 @ 9:57 AM
 * Copyright (C) 2010 Comala Technology Solutions, Inc.
 * confluence-dev@comalatech.com
 * All rights reserved.
 */
public class XHtmlMetadataListMacro extends MetadataListMacro {

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        String rawBody = super.execute(parameters,body,conversionContext);
        return renderString(conversionContext.getPageContext(),conversionContext.getEntity(),rawBody);
    }
}
