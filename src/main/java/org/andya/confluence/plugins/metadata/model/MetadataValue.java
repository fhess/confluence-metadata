/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.andya.confluence.utils.MathUtils;

import java.util.Date;

/**
 * This class represents a single piece of metadata.
 */
public class MetadataValue implements Comparable<MetadataValue> {
	private final String name;
	private final Object value;
	private final String wikiSnippet;

	public MetadataValue(String name, Object value, String wikiSyntax) {
		this.name = name;
		this.value = value;
		this.wikiSnippet = wikiSyntax;
	}

	public MetadataValue(String name, String wikiSyntax) {
		this.name = name;
		this.value = wikiSyntax;
		this.wikiSnippet = wikiSyntax;
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}

	public String getWikiSnippet() {
		return wikiSnippet;
	}

	public static String getWikiSnippet(MetadataValue value) {
		return value != null ? value.getWikiSnippet() : "";
	}

	public Number getValueAsNumber() {
		Object value = getValue();
		if (value instanceof Number)
			return (Number)value;
		else if (value instanceof String)
			return MathUtils.parseNumber((String)value);
		return null;
	}

	public Date getValueAsDate() {
		Object value = getValue();
		if (value instanceof Date)
			return (Date)value;
		return null;
	}

	public Boolean getValueAsBoolean() {
		Object value = getValue();
		if (value instanceof Boolean)
			return (Boolean)value;
		else if (value instanceof String) {
			boolean isTrue = "true".equalsIgnoreCase((String)value) || "y".equalsIgnoreCase((String)value);
			return Boolean.valueOf(isTrue);
		}
		return null;
	}

	public boolean equals(Object object) {
		if (this == object) return true;
		if (!(object instanceof MetadataValue)) return false;

		final MetadataValue metadataValue = (MetadataValue)object;

		if (name != null ? !name.equals(metadataValue.name) : metadataValue.name != null) return false;
		if (value != null ? !value.equals(metadataValue.value) : metadataValue.value != null) return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = (name != null ? name.hashCode() : 0);
		result = 29 * result + (value != null ? value.hashCode() : 0);
		return result;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public int compareTo(MetadataValue argument) {
		Date date = getValueAsDate();
		Date otherDate = argument.getValueAsDate();
		if (date != null && otherDate != null)
			return date.compareTo(otherDate);
		Number number = getValueAsNumber();
		Number otherNumber = argument.getValueAsNumber();
		if (number != null && otherNumber != null)
			return (int)(number.doubleValue() - otherNumber.doubleValue());
		return getWikiSnippet().compareTo(argument.getWikiSnippet());
	}
	
	public int compareValueAsIStringTo(MetadataValue argument) {
		String value1 = this.getValue().toString();
		String value2 = argument.getValue().toString();
		return value1.compareToIgnoreCase(value2);
	}

	public int compareValueAsStringTo(MetadataValue argument) {
		String value1 = this.getValue().toString();
		String value2 = argument.getValue().toString();
		return value1.compareTo(value2);
	}

	public int compareValueAsNumberTo(MetadataValue argument) {
		Number number = getValueAsNumber();
		Number otherNumber = argument.getValueAsNumber();
		if (number != null && otherNumber != null)
			return (int)(number.doubleValue() - otherNumber.doubleValue());

		// Fallback to default comparison
		return this.compareTo(argument);
	}

}
